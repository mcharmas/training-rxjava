package com.example;

import org.junit.Before;


public class GithubTests extends BaseObservableTest {

    private GitHubClient.GitHub gitHub;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        gitHub = GitHubClient.create("android-training", "androidtraining1");
    }
}
