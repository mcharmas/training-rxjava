package com.example;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FunctionalJavaTest {

    @Test
    public void testSampleStreamTests() throws Exception {
        //find the double of the first even number greater that 3
        int[] input = {1, 2, 3, 3, 7, 6, 5, 6, 7};

        int foundElement = findDoubleOfFirstEvenNumberGreaterThan3(input);

        assertEquals(12, foundElement);
    }

    private int findDoubleOfFirstEvenNumberGreaterThan3(int[] input) {
        return 0;
    }
}
