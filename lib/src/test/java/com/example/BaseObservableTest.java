package com.example;

import org.junit.After;
import org.junit.Before;

import io.reactivex.Observer;
import io.reactivex.observers.TestObserver;

public class BaseObservableTest {
    private TestObserver<?> subscriber;

    @Before
    public void setUp() throws Exception {
        System.out.println();
    }

    @After
    public void tearDown() throws Exception {
        if (subscriber != null) {
            subscriber.awaitTerminalEvent();
            subscriber = null;
        }
    }

    @SuppressWarnings("unchecked")
    public <T> Observer<T> wrap(Observer<T> subscriber) {
        TestObserver<T> decoratedObserver = new TestObserver<>(subscriber);
        this.subscriber = decoratedObserver;
        return decoratedObserver;
    }
}
