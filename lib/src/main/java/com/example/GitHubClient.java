package com.example;

import org.apache.commons.codec.binary.Base64;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class GitHubClient {
    private static final String API_URL = "https://api.github.com";

    public static class Contributor {
        public final String login;
        public final int contributions;

        public Contributor(String login, int contributions) {
            this.login = login;
            this.contributions = contributions;
        }
    }

    public static class Commit {
        public final String url;

        public Commit(String url) {
            this.url = url;
        }
    }

    interface GitHub {
        @GET("/repos/{owner}/{repo}/contributors?per_page=1000")
        Observable<List<Contributor>> contributors(
                @Path("owner") String owner,
                @Path("repo") String repo
        );

        @GET("/repos/{owner}/{repo}/commits?per_page=1000")
        Observable<List<Commit>> userCommits(
                @Path("owner") String owner,
                @Path("repo") String repo,
                @Query("author") String author
        );

    }

    public static GitHub create(String username, String password) {
        final String credentials = username == null || password == null ? null : username + ":" + password;

        OkHttpClient client = new OkHttpClient.Builder()
                .addNetworkInterceptor(chain -> {
                    if (credentials == null) return chain.proceed(chain.request());
                    Request requestWithAuthHeader = chain.request()
                            .newBuilder()
                            .addHeader("Authorization", "Basic " + Base64.encodeBase64String(credentials.getBytes()))
                            .build();

                    return chain.proceed(requestWithAuthHeader);
                })
                .build();

        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .client(client)
                .baseUrl(API_URL)
                .build()
                .create(GitHub.class);
    }
}
